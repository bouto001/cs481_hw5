﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace MyMapApp
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MapSettings MapSettings { get; set; }
        public MainPage()
        {
            InitializeComponent();

            MapSettings = new MapSettings();
            BindingContext = MapSettings;

            foreach (var pin in MapSettings.Pins)
                MyMap.Pins.Add(pin);

            MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(44.340769, 1.202578), Distance.FromMeters(300)));

        }

        private void Picker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = ((Picker)sender).SelectedItem.ToString();
            var pin = MapSettings.Pins.FirstOrDefault(x => x.Label == picker);

            MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(pin.Position, Distance.FromMeters(300)));
        }

        private void Picker_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            var picker = ((Picker)sender).SelectedItem.ToString();

            switch (picker)
            {
                case "Default":
                    MyMap.MapType = MapType.Street;
                    return;
                case "Satellite":
                    MyMap.MapType = MapType.Satellite;
                    return;
                case "Hybrid":
                    MyMap.MapType = MapType.Hybrid;
                    return;
            }

        }
    }
}
