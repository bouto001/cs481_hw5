﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Maps;

namespace MyMapApp
{
    public class MapSettings
    {
        public List<Pin> Pins { get; set; } = new List<Pin>();
        // 0 street, 1 satellite, 2 hybrid
        public MapType MapType { get; set; } = MapType.Street;

        public MapSettings()
        {
            Pins.Add(new Pin
            {
                Position = new Position(55.950837, -3.191274),
                Label = "Edinburgh",
                Address = "18 Market St, Edinburgh EH1 1BL, UK"
            });
            
            Pins.Add(new Pin
            {
                Position = new Position(37.833946, -122.477011),
                Label = "San Francisco",
                Address = "Sommerville Rd, Sausalito, CA 94965"
            }); 
            
            Pins.Add(new Pin
            {
                Position = new Position(48.693580, 6.183276),
                Label = "Nancy",
                Address = "Place Stanislas, 54000 Nancy, France"
            });


            Pins.Add(new Pin
            {
                Position = new Position(44.135528, 9.681328),
                Label = "Cinque Terre",
                Address = "Via Visconti, 19018 Vernazza SP, Italy"
            });
        }
    }
}
